<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        Eloquent::unguard();

        // call our class and run our seeds
        $this->call('DenunciasTableSeeder');
    }
}
