<?php

use Illuminate\Database\Seeder;

class DenunciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('denuncias')->delete();
        DB::table('categorias')->delete();
        DB::table('usuarios')->delete();
        DB::table('imagems')->delete();
        $this->command->info('limpando banco de dados');
        $usuario=\App\Usuario::firstOrCreate(array(
            "uuid"=>"genericUUID"
        ));
        for($i=0;$i<100;$i++) {
            $lat = rand(0, 210) / 10000.0;
            $lng = rand(0, 300) / 10000.0;
            $cat = rand(0, 2);
            $denuncia1 = \App\Denuncia::create(array(
                'texto' => 'minha denuncia',
                'lat' => -21.784348757848356 + $lat,
                'lng' => -43.373422622680664 + $lng,
                'categoria_id' => $cat,
                'usuario_uuid' => $usuario->uuid

            ));
            $url="";
            if($cat==0){
                $url="http://www.brummer.com.br/wp-content/uploads/2013/04/LIXO-NA-RUA1.jpg";
            }else if($cat==1){
                $url="http://www.pimenta.blog.br/wp-content/uploads/fotos-da-dengue-na-sede-da-dengue-002.jpg";
            }else{
                $url="http://1.bp.blogspot.com/-N_W_mhJCoFE/T2f8JzCed0I/AAAAAAAAABg/cqtdyQnSzGA/s1600/foto1+buracos.jpg";
            }


            \App\Imagem::create(array(
                'url' => $url,
                "denuncia_id" => $denuncia1->id
            ));
        }
    }
}
