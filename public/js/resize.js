$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

    // Prevent default anchor click behavior
    event.preventDefault()

	var a;
	
	if(event.toElement.hash == "#about"){ a = 45}
	else{
	if(event.toElement.hash == "#googleMap"){ a = 50}
	else{a=0}
	}

	
    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top - a
    }, 1500, function(){
   
      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
  });


  $("#main_mapa").css("height", $(window).height() - 55);
  $("#main_mapa").css("width", $(window).width());	
  
  $("#top_de_olho").css("height", $(window).height());


  $("#googleMap").css("height", $(window).height() - 55);
  $("#news_map").css("height", $(window).height() - 55);
  
  $("#googleMap").css("width", $(window).width()*(75/100.0));
  $("#news_map").css("width", $(window).width()*(25/100.0));

  
  $("#new_dad").css("max-height", $(window).height() - 85);
  $("#new_dad").css("height", $(window).height() - 85);
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
  $(window).resize(function() {
	  
	  $("#main_mapa").css("height", $(window).height() - 55);
	  $("#main_mapa").css("width", $(window).width());	
	  
	  $("#top_de_olho").css("height", $(window).height());


	  $("#googleMap").css("height", $(window).height() - 55);
	  $("#news_map").css("height", $(window).height() - 55);
	  
	  $("#googleMap").css("width", $(window).width()*(75/100.0));
	  $("#news_map").css("width", $(window).width()*(25/100.0));

	  
	  $("#new_dad").css("max-height", $(window).height() - 85);
	  $("#new_dad").css("height", $(window).height() - 85);

  });
})
