var map;
var box;
var markers = [];
var markerCluster;
var pontos;


var divPai = $('#new_dad');
var icon = ["img/ic_lixo","img/ic_dengue","img/ic_rua"];
var imagem = [
{
	url: "img/ic_lixo_mark.png",
	scaledSize: new google.maps.Size(24, 40),
},
{
	url: "img/ic_dengue_mark.png",
	scaledSize: new google.maps.Size(24, 40),
},
{
	url: "img/ic_rua_mark.png",
	scaledSize: new google.maps.Size(24, 40),
}
];


function initialize(){
	
	var latlng = new google.maps.LatLng(-21.7624,-43.3434);
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			latlng = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};


			//map.setCenter(latlng);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
	}
	var options = {
		zoom: 11,
		center:latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("googleMap"), options);
	
	google.maps.event.addListenerOnce(map, 'bounds_changed', function(){
		//carregarPontos();
		//draw_map();
	});
	
	google.maps.event.addListener(map, 'idle', function(){
		
		var c1 = map.getBounds().getNorthEast();
		var c2 = map.getBounds().getSouthWest();
		
		divPai.empty();
		markers = [];
		carregarPontos();
		//if(c1.lat() > box.getNorthEast().lat() || c1.lng() > box.getNorthEast().lng() || c2.lat() < box.getSouthWest().lat() || c2.lng() < box.getSouthWest().lng()){
		//	carregarPontos();
		//}
		
		//draw_map();
	
	});
	
}
// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}
function deleteMarkers() {
    clearMarkers();
    markers = [];
}
function carregarPontos() {
	
	
		box = map.getBounds();
		
		var c1 = box.getNorthEast();
		var c2 = box.getSouthWest();
					
		$.post("denuncia/getDenuncias", {
					lat1 : 180,
					lng1 : 180,
					lat2 : -180,
					lng2 : -180
				}, function(p){
            //markers=[];
            //deleteMarkers();
			pontos = JSON.parse(p);
			//console.log(pontos);
			draw_map();
		});
				
		
}

function draw_map(){

	var c1 = map.getBounds().getNorthEast();
	var c2 = map.getBounds().getSouthWest();

	$.each(pontos, function(index, ponto) {

		lat = parseFloat(ponto.lat)
		lng = parseFloat(ponto.lng)

		if(lat < c1.lat() && lat > c2.lat() && lng < c1.lng() && lng > c2.lng()){

			var marker = new google.maps.Marker({
				id: ponto.id,
				position: new google.maps.LatLng(lat, lng),
				title: "Denuncia: " + String(ponto.id),
				map: map,
				icon: imagem[parseInt(ponto.categoria_id)]
			});

			marker.addListener('click', function() {
				var ponto = pontos.filter(function(a){ if(a.id == marker.id) return a; })[0]
				divPai.empty();
				draw_div(ponto);
			});

			markers.push(marker);

			draw_div(ponto);
		}

	});

	var mcOptions = {zoomOnClick:false, minimumClusterSize:3, gridSize: 40, maxZoom: 17};
	markerCluster = new MarkerClusterer(map, markers, mcOptions);
}

function draw_div(ponto){

	var data = new Date(ponto.created_at);
	var stringData = ponto.created_at;
	var comentario = "";

	if(ponto.texto != ""){
		comentario = "<p>"+ponto.texto+"</p></div>"
	}




	divPai.append("<div id='news_"+ponto.id+"' class='col-xs-12 news'><figure>"
		+ "<img  class='img_denuncia img-responsive' src="+ponto.imagems[0].url+">"
		+ "<img  class='img_news' src='"+icon[parseInt(ponto.categoria_id)]+".png"+"'>"
		+ "<figcaption>"+ stringData
		+ "</figcaption>"
		+"</figure>"
		+ comentario
		+ "</div>"
	);

	var something = document.getElementById('news_'+ponto.id);

	something.style.cursor = 'pointer';
	something.onclick = function() {
		map.setZoom(19);

		lat = parseFloat(ponto.lat);
		lng = parseFloat(ponto.lng);
		myLatLng= new google.maps.LatLng(lat,lng);
		map.setCenter(myLatLng);
	};


}
$(document).ready(function(){
	google.maps.event.addDomListener(window, 'load', initialize);
});

