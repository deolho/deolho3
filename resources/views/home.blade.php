@extends('layouts.app')
@section('javaScript')
    <script>
        $(document).ready(function(){
            console.log("teste");
            getAllDenuncias();

        });
        function aprovarDenuncia(id){
            $.post("denuncia/aprovarDenuncia", { denuncia_id:id}, function(p){
                console.log(p);
                $("#denuncia-"+id).html("OK =]");
            });
        }
        function aprovarDenunciaTweet(id){
            $.post("denuncia/aprovarDenunciaTweet", { denuncia_id:id}, function(p){
                console.log(p);
                $("#denuncia-"+id).html("OK =]");
            });
        }
        function revomeDenuncia(id){
            $.post("denuncia/removeDenuncia", { denuncia_id:id}, function(p){
                console.log(p);
                $("#denuncia-"+id).html("Removed");
            });
        }
        function getAllDenuncias(){
            $.get("denuncia/allDenuncia", function(p){

                denuncias = JSON.parse(p);
                console.log(denuncias);
                s=''
                for( var i in denuncias ){
                    console.log(denuncias[i]);
                    s+='<div id="denuncia-'+denuncias[i].id+'"  class="well well-lg" > ';
                    s+='<h3 > '+denuncias[i].texto+'</h3>';
                    s+='<img id="picture" src="'+denuncias[i].imagems[0].url+ '" class="img-responsive hidden-sm hidden-xs news-image" >';
                    s+='<button type="button" class="btn btn-danger" value="'+denuncias[i].id+'" onclick=" revomeDenuncia('+denuncias[i].id+')" >Remover</button>';
                    s+='<button type="button" class="btn btn-primary" value="'+denuncias[i].id+'" onclick=" aprovarDenuncia('+denuncias[i].id+')" >Ok</button>';
                    s+='<button type="button" class="btn btn-primary" value="'+denuncias[i].id+'" onclick=" aprovarDenunciaTweet('+denuncias[i].id+')" >Ok - tweet </button>';
                    s+='</div>'
                }
                $("#main_windown").html(s)
            });
        }

    </script>
@endsection
@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <div id="main_windown">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
