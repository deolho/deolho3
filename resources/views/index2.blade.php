<!DOCTYPE html>

<head>

    <title>DeOlho</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ URL::asset('js/markerclusterer.js') }}"></script>
    {{--<script>--}}

    {{--$(document).ready(function(){--}}
    {{--console.log("teste");--}}
    {{--getAllDenuncias();--}}

    {{--});--}}
    {{--</script>--}}
    {{--<script>--}}

    {{--var script = '<script type="text/javascript" src="{{ URL::asset('js/markerclusterer') }}"';--}}
    {{--if (document.location.search.indexOf('compiled') !== -1) {--}}
    {{--script += '_compiled';--}}
    {{--}--}}
    {{--script += '.js"><' + '/script>';--}}

    {{--document.write(script);--}}
    {{--</script>--}}


</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">DeOlho</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#googleMap">Mapa</a></li>
                <li><a href="#about">Sobre</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="top_de_olho" class="jumbotron text-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-10 col-centered col-centered">
                <img src="img/deOlho_logo.png " class="img-responsive col-centered" style="width:50%;height:50%">
            </div>
        </div>


        <div class="row">


            <div class="col-sm-4 ">

                <a href="https://play.google.com/store/apps/details?id=arca.com.br.deolho" target="_blank"><img
                            id="google_play" style="max-width: 60%;" class='col-centered img-responsive'
                            src="img/google_play.png"></a>


            </div>
        </div>
    </div>
</div>
<div id=map-content" class="hidden-xs hidden-sm">
<div id="main_mapa">
    <div class="row" style="padding:0px; margin:0px;">

        <div class=" hidden-xs hidden-sm col-xs-8" id="googleMap"></div>

        <div class="hidden-xs hidden-sm col-xs-4" id="news_map">
            <div class="pre-scrollable " id="new_dad"></div>
        </div>

    </div>
</div>
</div>
<!-- Add Google Maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    denuncia_lat= {{$denuncia[0]->lat}};
    denuncia_lng= {{$denuncia[0]->lng}};
//    console.log(denuncia_lat);
</script>
<!-- Arquivo de inicialização do mapa -->
<script src="js/mapa_denuncia.js"></script>

<!-- Container (About Section) -->
<div id="about" class="container-fluid bg-grey">

    <div class="row">
        <div class="col-sm-8">
            <h2>Sobre DeOlho</h2><br>
            <h4 style="text-align: justify">DeOlho é um aplicativo capaz de informar locais com problemas urbanos,
                descartes de lixo de maneira incorreta e impropria e focos de dengue, através de fotos e localização via
                GPS. Com as informações coletadas por toda a cidade, pode ser feito um mapa de focos do problema,
                tornando o trabalho dos órgãos responsáveis mais direcionado.</h4><br>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-globe logo slideanim"></span>
        </div>
    </div>

    <div class="row ">
        <div class="col-xs-6 ">

            <a href="https://www.facebook.com/AppDeOlho" target="_blank"><img id="facebook" style="height:140px"
                                                                              class='col-centered img-responsive'
                                                                              src="img/facebook.png"></a>


        </div>
        <div class="col-xs-6 hidden-xs hidden-sm">

            <a href="https://play.google.com/store/apps/details?id=arca.com.br.deolho" target="_blank"><img
                        id="google_play" style="height:140px" class='col-centered img-responsive'
                        src="img/google_play.png"></a>


        </div>
    </div>

</div>


<footer class="container-fluid text-center">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <p>Fala-se tanto da necessidade de deixar um planeta melhor para os nossos filhos e, esquece-se da urgência de
        deixarmos filhos melhores para o nosso planeta.</p>
</footer>

<script src="js/resize.js"></script>

</body>
</html>
