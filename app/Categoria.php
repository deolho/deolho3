<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    //
    protected $fillable = array('tipo');

    // DEFINE RELATIONSHIPS --------------------------------------------------



    // each denuncia climbs many Images
    public function Denuncia() {
        return $this->hasMany('Denuncia');
    }
}
