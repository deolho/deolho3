<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //
    protected $fillable = array('texto','type','email','usuario_uuid');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each denuncia HAS one categoria to eat

    public function usuario() {
        return $this->hasOne('App\Usuario');
    }


}
