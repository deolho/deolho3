<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested. /{lng1}/{lat2}/{lng2}
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/email', function () {
    $email=Mail::send('email.email', ['user' => 'jmarcosdo@gmail.com'], function ($m)  {
        $m->from('contato@deolho.xyz', 'Your Application');
//        dd($m);
        $m->to("twitter@deolho.xyz")->subject('Your Reminder!');
//        dd($m);
    });
    dd($email);
    return view('index');
});
Route::get('/play', function () {
    return redirect('https://play.google.com/store/apps/details?id=arca.com.br.deolho');
});

Route::post ( 'denuncia/getDenuncias/', "DenunciaController@getDenuncias" );
Route::post ( 'denuncia/saveDenuncia/', "DenunciaController@saveDenuncias" );
Route::post ( 'report/saveReport/', "ReportController@saveReport" );




Route::post ( 'denuncia/removeDenuncia/',  [
    'middleware' => 'auth.basic',
    'uses' => "UsuariosController@removeDenuncias"
]);
Route::post ( 'denuncia/aprovarDenuncia/',  [
    'middleware' => 'auth.basic',
    'uses' => "UsuariosController@aprovarDenuncia"
]);
Route::post ( 'denuncia/aprovarDenunciaTweet/',  [
    'middleware' => 'auth.basic',
    'uses' => "UsuariosController@aprovarDenunciaTweet"
]);
Route::get ( 'denuncia/allDenuncia/',  [
    'middleware' => 'auth.basic',
    'uses' => "UsuariosController@getAllDenuncias"
]);



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
//    Route::auth();
//
//    Route::post ( 'denuncia/allDenuncia/', "UsuariosController@getAllDenuncias" );
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get ( 'denuncia/allDenuncia/', "UsuariosController@getAllDenuncias" );
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/cidade', 'CidadeController@index');
});
Route::get('/{id}',  "DenunciaController@getDenunciasid" );