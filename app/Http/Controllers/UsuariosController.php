<?php

namespace App\Http\Controllers;

use App\Denuncia;
use App\Imagem;
use App\Usuario;
use Illuminate\Http\Request;
use \Twitter;
use \File;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    public function removeDenuncias(Request $request) {
        if($request->user()) {
            $id = $request->input('denuncia_id');


            Denuncia::where('id', '=', $id)->delete();
            Imagem::where('denuncia_id', '=', $id)->delete();
            return $id;
        }
//        $denuncias= Denuncia::all();  ->where(`lat`,'>',$lat2)-> where(`lng`,'<',$lng1)->where(`lng`,'>',$lng2)

    }
    public function aprovarDenuncia(Request $request) {
        if($request->user()) {
            $id = $request->input('denuncia_id');


            $denuncia=Denuncia::where('id', '=', $id)->get();
            $denuncia[0]->verificado=1;
            $denuncia[0]->save();

            return $id;
        }
//        $denuncias= Denuncia::all();  ->where(`lat`,'>',$lat2)-> where(`lng`,'<',$lng1)->where(`lng`,'>',$lng2)

    }
    public function aprovarDenunciaTweet(Request $request) {
        if($request->user()) {
            $id = $request->input('denuncia_id');


            $denuncia=Denuncia::where('id', '=', $id)->get();
            $denuncia[0]->verificado=1;
            $denuncia[0]->imagems;
//            dd($denuncia[0]->imagems[0]->url);
            $status= substr ("http://deolho.xyz/".$denuncia[0]->id."  : ".$denuncia[0]->texto , 0 ,120 );
            $uploaded_media = Twitter::uploadMedia(['media' => File::get('/home/u701086596/public_html'.$denuncia[0]->imagems[0]->url)]);
            $t= Twitter::postTweet(['status' => $status,'lat'=>$denuncia[0]->lat,'long'=>$denuncia[0]->lng, 'media_ids' => $uploaded_media->media_id_string]);


            $denuncia[0]->save();
            $id=$denuncia[0]->id;
            return $id;
        }
//        $denuncias= Denuncia::all();  ->where(`lat`,'>',$lat2)-> where(`lng`,'<',$lng1)->where(`lng`,'>',$lng2)

    }
    public function getAllDenuncias(Request $request){
        if($request->user()) {
            $denuncias = Denuncia::where('verificado', '=', 0)->get();
            foreach ($denuncias as $denuncia)
                $denuncia->imagems;

            return json_encode($denuncias);
        }else{

            return [];
        }

    }
}
