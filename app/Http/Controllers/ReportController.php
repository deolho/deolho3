<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use App\Usuario;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    //
    public function saveReport(Request $request) {
        //
        $usuario_uuid = $request->input("usuario_uuid");
        $report_texto = $request->input("report_texto");
        $report_type = $request->input("report_type");
        $report_email="";
        if($request->input("report_email"))
            $report_email=$request->input("report_email");

        $usuario=Usuario::firstOrCreate(['uuid' => $usuario_uuid]);
        $denuncia1 = Report::create(array(
            'texto' => $report_texto ,
            'type' => $report_type,
            'email' => $report_email,
            'usuario_uuid'=>$usuario_uuid

        ));
        return json_encode($denuncia1);

    }
}
