<?php

namespace App\Http\Controllers;

use App\Denuncia;
use App\Imagem;
use App\Usuario;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DenunciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view ( "index" );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * save a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveDenuncias(Request $request) {
        //
        $usuario_uuid = $request->input("usuario_uuid");
        $denuncia_text = $request->input("denuncia_texto");
        $denuncia_imagem = $request->input("denuncia_imagem");
        $denuncia_lat = $request->input("denuncia_lat");
        $denuncia_lng = $request->input("denuncia_lng");
        $categoria_id =  $request->input("denuncia_categoria");
        $endereco = '';
        if($request->input("denuncia_endereco")!=null) {
            $endereco = $request->input("denuncia_endereco");
        }
        $usuario=Usuario::firstOrCreate(['uuid' => $usuario_uuid]);
        $denuncia = Denuncia::create(array(
            'texto' => $denuncia_text ,
            'lat' => $denuncia_lat,
            'lng' => $denuncia_lng,
            'categoria_id' => $categoria_id,
            'usuario_uuid' => $usuario->uuid,
            'endereco'=>$endereco

        ));
        $img="/img/denuncias/denuncia-".time().".png";
        $url=public_path() .$img;
        $ifp = fopen($url, "wb");

//        $data = explode(',', $denuncia_imagem);

        fwrite($ifp, base64_decode($denuncia_imagem));
        fclose($ifp);
        $imagem=Imagem::create(array(
            'url' => "/public".$img,
            "denuncia_id" => $denuncia->id
        ));
//        dd($imagem);
        $denuncia->imagems;
        return json_encode($denuncia);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    //
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response,$lng1,$lat2,$lng2
     */
    public function getDenuncias(Request $request) {
        $lat1=$request->input('lat1');
        $lat2=$request->input('lat2');
        $lng1=$request->input('lng1');
        $lng2=$request->input('lng2');


       $denuncias= Denuncia::where('lat','<',$lat1)->where('lat','>',$lat2)-> where('lng','<',$lng1)->where('lng','>',$lng2)->orderBy('created_at', 'desc')->get();

//        $denuncias= Denuncia::all();  ->where(`lat`,'>',$lat2)-> where(`lng`,'<',$lng1)->where(`lng`,'>',$lng2)
        foreach ($denuncias as $denuncia)
            $denuncia->imagems;
//        dd($denuncias);
        return json_encode( $denuncias );
    }


    public function getDenunciasid($id) {
//        $id=$request->input('id');



        $denuncias= Denuncia::where('id', '=', $id)->get();

//        $denuncias= Denuncia::all();  ->where(`lat`,'>',$lat2)-> where(`lng`,'<',$lng1)->where(`lng`,'>',$lng2)
        foreach ($denuncias as $denuncia)
            $denuncia->imagems;

//        dd($id);
//        dd($denuncias);
        if(count($denuncias)==0)
            return view('index');
        return view('index2')->with('denuncia',$denuncias);
    }
}
