<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    //
    protected $fillable = array('texto','lat','lng','categoria_id','usuario_uuid','endereco');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each denuncia HAS one categoria to eat
    public function fish() {
        return $this->belongsTo('Categoria');
    }
    // each denuncia HAS one categoria to eat
    public function usuario() {
        return $this->hasOne('App\Usuario');
    }

    // each denuncia climbs many Imagem
    public function imagems() {
        return $this->hasMany('App\Imagem');
    }


}
