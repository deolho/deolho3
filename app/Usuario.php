<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $fillable = array('uuid');

    // DEFINE RELATIONSHIPS --------------------------------------------------


    // each Usuario climbs many Denuncia
    public function denuncia() {
        return $this->hasMany('App\Denuncia');
    }
    public function report() {
        return $this->hasMany('App\Report');
    }
}
