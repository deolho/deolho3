<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagem extends Model
{
    protected $fillable = array('url','denuncia_id');

    // DEFINE RELATIONSHIPS --------------------------------------------------


    // each Imagem climbs belongs to one Denuncia
    public function denuncia() {
        return $this->belongsTo('Denuncia');
    }
}
